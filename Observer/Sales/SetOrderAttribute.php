<?php
declare(strict_types=1);

namespace Mastering\LuxuryTax\Observer\Sales;

use Exception;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Mastering\LuxuryTax\Model\LuxuryTaxRepository;

class SetOrderAttribute implements ObserverInterface
{

    /**
     * @var LuxuryTaxRepository
     */
    private LuxuryTaxRepository $luxuryTaxRepository;

    /**
     * @param LuxuryTaxRepository $luxuryTaxRepository
     */
    public function __construct(
        LuxuryTaxRepository $luxuryTaxRepository
    )
    {
        $this->luxuryTaxRepository = $luxuryTaxRepository;
    }

    /**
     * @param Observer $observer
     * @return void
     * @throws NoSuchEntityException
     */
    public function execute(
        Observer $observer
    )
    {
        $order = $observer->getData('order');
        $customerGroupId = $order->getCustomerGroupId();
        try {
            $luxuryTax = $this->luxuryTaxRepository->getByCustomerGroup((int)$customerGroupId);
            $tax = ((float)$order->getSubtotal() * (float)$luxuryTax->getConditionAmount()) / 100;
            $order->setLuxuryTax($tax);
            $tax = ((float)$order->getBaseSubtotal() * (float)$luxuryTax->getConditionAmount()) / 100;
            $order->setBaseLuxuryTax($tax);
        } catch (Exception $exception) {
            $tax = 0;
            $order->setLuxuryTax($tax);
            $order->setBaseLuxuryTax($tax);
        }

        $order->save();
    }
}
