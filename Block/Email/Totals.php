<?php
declare(strict_types=1);

namespace Mastering\LuxuryTax\Block\Email;

use Magento\Framework\DataObject;
use Magento\Framework\View\Element\Template;

class Totals extends Template
{
    /**
     * Initialize all order totals relates with tax
     */
    public function initTotals()
    {
        /** @var \Magento\Sales\Block\Order\Totals $orderTotalsBlock */
        $orderTotalsBlock = $this->getParentBlock();
        $order = $orderTotalsBlock->getOrder();
        $orderTotalsBlock->addTotal(new DataObject([
            'code' => 'luxury_tax',
            'label' => __('Luxury Tax'),
            'value' => $order->getLuxuryTax(),
            'base_value' => $order->getBaseLuxuryTax(),
        ]), 'subtotal');
    }
}