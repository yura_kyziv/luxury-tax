<?php
declare(strict_types=1);

namespace Mastering\LuxuryTax\Block\Adminhtml\Order;

use Magento\Backend\Block\Template;
use Magento\Backend\Block\Template\Context;
use Mastering\LuxuryTax\Helper\Data;

class LuxuryTaxColumnColor extends Template
{
    /**
     * @var Data
     */
    public Data $helper;
    /**
     * Block template.
     *
     * @var string
     */
    protected $_template = 'order/luxuryTaxColor.phtml';

    /**
     * AssignProducts constructor.
     *
     * @param Context $context
     * @param Data $helper
     * @param array $data
     */
    public function __construct(
        Context $context,
        Data    $helper,
        array   $data = []
    )
    {
        $this->helper = $helper;
        parent::__construct($context, $data);
    }
}