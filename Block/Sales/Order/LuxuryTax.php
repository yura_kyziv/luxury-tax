<?php
declare(strict_types=1);

namespace Mastering\LuxuryTax\Block\Sales\Order;

use Magento\Framework\DataObject;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Sales\Block\Order\Totals;
use Magento\Sales\Model\Order;
use Magento\Tax\Model\Config;

/**
 * Tax totals modification block. Can be used just as subblock of \Magento\Sales\Block\Order\Totals
 */
class LuxuryTax extends Template
{
    /**
     * Tax configuration model
     *
     * @var Config
     */
    protected Config $_config;

    /**
     * @var Order
     */
    protected Order $_order;

    /**
     * @var DataObject
     */
    protected DataObject $_source;

    /**
     * @param Context $context
     * @param Config $taxConfig
     * @param array $data
     */
    public function __construct(
        Context $context,
        Config  $taxConfig,
        array   $data = []
    )
    {
        $this->_config = $taxConfig;
        parent::__construct($context, $data);
    }

    /**
     * Check if we nedd display full tax total info
     *
     * @return bool
     */
    public function displayFullSummary()
    {
        return true;
    }

    public function getStore()
    {
        return $this->_order->getStore();
    }

    /**
     * Initialize all order totals relates with tax
     *
     */
    public function initTotals()
    {

        /** @var Totals $parent */
        $parent = $this->getParentBlock();
        $this->_order = $parent->getOrder();
        $this->_source = $parent->getSource();

        $fee = new DataObject(
            [
                'code' => 'luxury_tax',
                'strong' => false,
                'value' => $this->getConditionAmount(),
                'base_value' => $this->getBaseConditionAmount(),
                'label' => __('Luxury tax'),
            ]
        );
        $parent->addTotal($fee, 'luxuryTax');

        return $this;
    }

    /**
     * @return Order
     */
    public function getOrder(): Order
    {
        return $this->_order;
    }

    /**
     * Get data (totals) source model
     *
     * @return DataObject
     */
    public function getSource()
    {
        return $this->_source;
    }

    /**
     * @return string
     * @throws NoSuchEntityException
     */
    private function getConditionAmount(): string
    {
        $order = $this->getOrder();
        return $order->getLuxuryTax();
    }

    /**
     * @return string
     * @throws NoSuchEntityException
     */
    private function getBaseConditionAmount(): string
    {
        $order = $this->getOrder();
        return $order->getBaseLuxuryTax();
    }
}
