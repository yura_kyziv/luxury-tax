<?php
declare(strict_types=1);

namespace Mastering\LuxuryTax\Block\Grid;

use Magento\Backend\Block\Widget\Grid\Column\Renderer\AbstractRenderer;
use Magento\Framework\DataObject;

class LuxuryTax extends AbstractRenderer
{

    /**
     * Format luxury tax for grid
     *
     * @param DataObject $row
     * @return string
     */
    public function render(DataObject $row): string
    {
        return '$' . number_format((float)$row->getBaseLuxuryTax(), 2, '.', '');
    }
}