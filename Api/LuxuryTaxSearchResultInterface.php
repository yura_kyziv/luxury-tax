<?php
declare(strict_types=1);

namespace Mastering\LuxuryTax\Api;

use Magento\Framework\Api\SearchResultsInterface;

interface LuxuryTaxSearchResultInterface extends SearchResultsInterface
{
    /**
     * @return \Mastering\LuxuryTax\Api\Data\LuxuryTaxInterface[]
     */
    public function getItems();

    /**
     * @param \Mastering\LuxuryTax\Api\Data\LuxuryTaxInterface[] $items
     * @return void
     */
    public function setItems(array $items);

}
