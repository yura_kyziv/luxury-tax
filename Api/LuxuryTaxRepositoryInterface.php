<?php
declare(strict_types=1);

namespace Mastering\LuxuryTax\Api;

use Magento\Framework\Api\SearchCriteriaInterface;
use Mastering\LuxuryTax\Api\Data\LuxuryTaxInterface;
use Mastering\LuxuryTax\Model\LuxuryTax;

interface LuxuryTaxRepositoryInterface
{
    /**
     * @param int $id
     * @return LuxuryTaxInterface
     */
    public function get(int $id): LuxuryTaxInterface;

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return LuxuryTaxSearchResultInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria): LuxuryTaxSearchResultInterface;

    /**
     * @param LuxuryTax $luxuryTax
     * @return LuxuryTaxInterface
     */
    public function save(LuxuryTax $luxuryTax): LuxuryTaxInterface;

    /**
     * @param LuxuryTax $workingHours
     * @return bool
     */
    public function delete(LuxuryTax $workingHours): bool;
}

