<?php
declare(strict_types=1);

namespace Mastering\LuxuryTax\Api\Data;

/**
 * Interface for LuxuryTax model
 */
interface LuxuryTaxInterface
{

    /**
     * Id attribute name
     *
     * @var string
     */
    const ID = 'id';

    /**
     * Status attribute name
     *
     * @var string
     */
    const STATUS = 'status';

    /**
     * Name attribute name
     *
     * @var string
     */
    const NAME = 'name';

    /**
     * Description attribute name
     *
     * @var string
     */
    const DESCRIPTION = 'description';

    /**
     * Customer group attribute name
     *
     * @var string
     */
    const CUSTOMER_GROUP = 'customer_group';

    /**
     * Tax rate attribute name
     *
     * @var string
     */
    const TAX_RATE = 'tax_rate';

    /**
     * Condition amount attribute name
     *
     * @var string
     */
    const  CONDUCTION_AMOUNT = 'condition_amount';

    /**
     * @return mixed|null
     */
    public function getId();

    /**
     * @param mixed $value
     * @return LuxuryTaxInterface
     */
    public function setId($value): LuxuryTaxInterface;

    /**
     * @return bool
     */
    public function getStatus(): bool;

    /**
     * @param bool $status
     * @return void
     */
    public function setStatus(bool $status): void;

    /**
     * @return string
     */
    public function getTaxName(): string;

    /**
     * @param string $taxName
     * @return void
     */
    public function setTaxName(string $taxName): void;

    /**
     * @return string
     */
    public function getDescription(): string;

    /**
     * @param string $description
     * @return void
     */
    public function setDescription(string $description): void;

    /**
     * @return int
     */
    public function getCustomerGroup(): int;

    /**
     * @param int $customerGroup
     * @return void
     */
    public function setCustomerGroup(int $customerGroup): void;

    /**
     * @return float
     */
    public function getConditionAmount(): float;

    /**
     * @param float $conditionAmount
     * @return void
     */
    public function setConditionAmount(float $conditionAmount): void;

    /**
     * @return int
     */
    public function getTaxRate(): int;

    /**
     * @param int $taxRate
     * @return void
     */
    public function setTaxRate(int $taxRate): void;
}
