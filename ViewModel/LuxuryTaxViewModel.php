<?php
declare(strict_types=1);

namespace Mastering\LuxuryTax\ViewModel;

use Magento\Customer\Api\Data\CustomerInterface;
use Magento\Customer\Model\ResourceModel\CustomerRepository;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use Mastering\LuxuryTax\Api\Data\LuxuryTaxInterface;
use Mastering\LuxuryTax\Model\LuxuryTaxRepository;

class LuxuryTaxViewModel implements ArgumentInterface
{

    /**
     * @var LuxuryTaxRepository
     */
    private LuxuryTaxRepository $luxuryTaxRepository;

    /**
     * @var Http
     */
    private Http $request;

    /**
     * @var CustomerRepository
     */
    private CustomerRepository $customerRepository;

    /**
     * @param Http $request
     * @param LuxuryTaxRepository $luxuryTaxRepository
     * @param CustomerRepository $customerRepository
     */
    public function __construct(
        Http                $request,
        LuxuryTaxRepository $luxuryTaxRepository,
        CustomerRepository  $customerRepository
    )
    {
        $this->request = $request;
        $this->luxuryTaxRepository = $luxuryTaxRepository;
        $this->customerRepository = $customerRepository;
    }

    /**
     * @param int $groupId
     * @return LuxuryTaxInterface
     * @throws NoSuchEntityException
     */
    public function getTax(int $groupId): LuxuryTaxInterface
    {
        $data = $this->luxuryTaxRepository->getByCustomerGroup((int)$groupId);
        return $data;
    }

    /**
     * @return CustomerInterface
     * @throws NoSuchEntityException
     * @throws LocalizedException
     */
    public function getUserByParams(): CustomerInterface
    {
        $this->request->getParams();
        $id = (int)$this->request->getParam('id');
        $customer = $this->customerRepository->getById($id);
        return $customer;
    }
}
