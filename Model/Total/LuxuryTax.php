<?php
declare(strict_types=1);

namespace Mastering\LuxuryTax\Model\Total;

use Exception;
use Magento\Customer\Model\Session;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Quote\Api\Data\ShippingAssignmentInterface;
use Magento\Quote\Model\Quote;
use Magento\Quote\Model\Quote\Address\Total;
use Magento\Quote\Model\Quote\Address\Total\AbstractTotal;
use Magento\Quote\Model\QuoteValidator;
use Mastering\LuxuryTax\Api\Data\LuxuryTaxInterface;
use Mastering\LuxuryTax\Model\LuxuryTaxRepository;

class LuxuryTax extends AbstractTotal
{

    /**
     * @var QuoteValidator|null
     */
    protected $quoteValidator = null;

    /**
     * @var Session
     */
    private Session $session;

    /**
     * @var LuxuryTaxRepository
     */
    private LuxuryTaxRepository $luxuryTaxRepository;

    /**
     * @param QuoteValidator $quoteValidator
     * @param Session $session
     * @param LuxuryTaxRepository $luxuryTaxRepository
     */
    public function __construct(
        QuoteValidator      $quoteValidator,
        Session             $session,
        LuxuryTaxRepository $luxuryTaxRepository
    )
    {
        $this->quoteValidator = $quoteValidator;
        $this->session = $session;
        $this->luxuryTaxRepository = $luxuryTaxRepository;
    }

    /**
     * Collect grand total address amount
     *
     * @param Quote $quote
     * @param ShippingAssignmentInterface $shippingAssignment
     * @param Total $total
     * @return $this
     */
    public function collect(
        Quote                       $quote,
        ShippingAssignmentInterface $shippingAssignment,
        Total                       $total
    )
    {
        parent::collect($quote, $shippingAssignment, $total);

        $tax = $this->calculateLuxuryTax($quote);
        $baseTax = $this->calculateBaseLuxuryTax($quote);

        $total->addBaseTotalAmount('luxuryTax', $baseTax);
        $total->setTotalAmount('luxuryTax', $tax);

        return $this;
    }

    /**
     * @param Quote $quote
     * @return float
     */
    private function calculateLuxuryTax(Quote $quote)
    {
        try {
            $tax = $this->getLuxuryTax();
            if ($tax->getStatus()) {
                $conditionAmount = $tax->getConditionAmount();
            } else {
                return 0;
            }
        } catch (Exception $e) {
            return 0;
        }

        $result = ($quote->getSubtotal() * $conditionAmount) / 100;
        return $result;
    }

    /**
     * @return LuxuryTaxInterface
     * @throws NoSuchEntityException
     */
    private function getLuxuryTax()
    {
        $session = $this->session;
        $isLogin = $session->isLoggedIn();

        if ($isLogin) {
            $customerGroupId = $this->session->getCustomer()->getGroupId();
        } else {
            $customerGroupId = 0;
        }

        $luxuryTax = $this->luxuryTaxRepository->getByCustomerGroup((int)$customerGroupId);

        return $luxuryTax;
    }

    /**
     * @param Quote $quote
     * @return float
     */
    private function calculateBaseLuxuryTax(Quote $quote)
    {
        try {
            $tax = $this->getLuxuryTax();
            if ($tax->getStatus()) {
                $conditionAmount = $tax->getConditionAmount();
            } else {
                return 0;
            }
        } catch (Exception $e) {
            return 0;
        }

        $result = ($quote->getBaseSubtotal() * $conditionAmount) / 100;
        return $result;
    }

    /**
     * Assign subtotal amount and label to address object
     *
     * @param Quote $quote
     * @param Total $total
     * @return array
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function fetch(Quote $quote, Total $total): array
    {
        return [
            'code' => 'luxuryTax',
            'title' => 'luxuryTax',
            'value' => $this->calculateLuxuryTax($quote),
            'base_value' => $this->calculateBaseLuxuryTax($quote)
        ];
    }

    /**
     * Get Subtotal label
     *
     * @return string
     */
    public function getLabel(): string
    {
        return __('Luxury tax')->render();
    }
}
