<?php
declare(strict_types=1);

namespace Mastering\LuxuryTax\Model\Total;

use Magento\Sales\Model\Order\Invoice as OrderInvoice;
use Magento\Sales\Model\Order\Invoice\Total\AbstractTotal;

class Invoice extends AbstractTotal
{
    /**
     * Collect invoice subtotal
     *
     * @param OrderInvoice $invoice
     * @return $this
     */
    public function collect(OrderInvoice $invoice)
    {
        $luxuryTax = $invoice->getOrder()->getLuxuryTax();
        $baseLuxuryTax = $invoice->getOrder()->getBaseLuxuryTax();
        $invoice->setGrandTotal($invoice->getGrandTotal() + $luxuryTax);
        $invoice->setBaseGrandTotal($invoice->getBaseGrandTotal() + $baseLuxuryTax);
        return $this;
    }
}
