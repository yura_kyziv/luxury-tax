<?php
declare(strict_types=1);

namespace Mastering\LuxuryTax\Model;

use Exception;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\StateException;
use Mastering\LuxuryTax\Api\Data\LuxuryTaxInterface;
use Mastering\LuxuryTax\Api\LuxuryTaxRepositoryInterface;
use Mastering\LuxuryTax\Api\LuxuryTaxSearchResultInterface;
use Mastering\LuxuryTax\Api\LuxuryTaxSearchResultInterfaceFactory;
use Mastering\LuxuryTax\Model\ResourceModel\LuxuryTax as LuxuryTaxResource;
use Mastering\LuxuryTax\Model\ResourceModel\LuxuryTax\CollectionFactory;

class LuxuryTaxRepository implements LuxuryTaxRepositoryInterface
{
    /**
     * @var CollectionFactory
     */
    private CollectionFactory $collectionFactory;

    /**
     * @var LuxuryTaxResource
     */
    private LuxuryTaxResource $luxuryTaxResource;

    /**
     * @var LuxuryTaxFactory
     */
    private LuxuryTaxFactory $luxuryTaxFactory;

    /**
     * @var LuxuryTaxSearchResultInterfaceFactory
     */
    private LuxuryTaxSearchResultInterfaceFactory $searchResultFactory;

    /**
     * @param LuxuryTaxFactory $luxuryTaxFactory
     * @param CollectionFactory $collectionFactory
     * @param LuxuryTaxResource $luxuryTax
     * @param LuxuryTaxSearchResultInterfaceFactory $searchResultFactory
     */
    public function __construct(
        LuxuryTaxFactory                      $luxuryTaxFactory,
        CollectionFactory                     $collectionFactory,
        LuxuryTaxResource                     $luxuryTax,
        LuxuryTaxSearchResultInterfaceFactory $searchResultFactory
    )
    {
        $this->luxuryTaxFactory = $luxuryTaxFactory;
        $this->collectionFactory = $collectionFactory;
        $this->luxuryTaxResource = $luxuryTax;
        $this->searchResultFactory = $searchResultFactory;
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return LuxuryTaxSearchResultInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria): LuxuryTaxSearchResultInterface
    {
        $collection = $this->collectionFactory->create();
        foreach ($searchCriteria->getFilterGroups() as $filterGroup) {
            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
                $collection->addFieldToFilter($filter->getField(), [$condition => $filter->getValue()]);
            }
        }

        $searchResult = $this->searchResultFactory->create();
        $searchResult->setSearchCriteria($searchCriteria);
        /** @var LuxuryTaxInterface[] $items */
        $items = $collection->getItems();
        $searchResult->setItems($items);
        $searchResult->setTotalCount($collection->getSize());
        return $searchResult;
    }

    /**
     * @param LuxuryTax $luxuryTax
     * @return LuxuryTaxInterface
     * @throws AlreadyExistsException
     */
    public function save(LuxuryTax $luxuryTax): LuxuryTaxInterface
    {
        $this->luxuryTaxResource->save($luxuryTax);
        return $luxuryTax;
    }

    /**
     * @param string $id
     * @return bool
     * @throws NoSuchEntityException
     * @throws StateException
     */
    public function deleteById(string $id): bool
    {
        try {
            /** @var LuxuryTax $item */
            $item = $this->get((int)$id);
            return $this->delete($item);
        } catch (NoSuchEntityException $e) {
            throw new $e();
        }
    }

    /**
     * @param int $id
     * @return LuxuryTaxInterface
     * @throws NoSuchEntityException
     */
    public function get(int $id): LuxuryTaxInterface
    {
        $object = $this->luxuryTaxFactory->create();
        $this->luxuryTaxResource->load($object, $id);
        if (!$object->getId()) {
            throw new NoSuchEntityException(__("Unable to find entity with ID {$id}"));
        }
        return $object;
    }

    /**
     * @param LuxuryTax $workingHours
     * @return bool
     * @throws StateException
     */
    public function delete(LuxuryTax $workingHours): bool
    {
        try {
            $this->luxuryTaxResource->delete($workingHours);
        } catch (Exception $e) {
            throw new StateException(__("Unable to remove entity #{$workingHours->getId()}"));
        }
        return true;
    }

    /**
     * @param int $id
     * @return LuxuryTaxInterface
     * @throws NoSuchEntityException
     */
    public function getByCustomerGroup(int $id): LuxuryTaxInterface
    {
        $object = $this->luxuryTaxFactory->create();
        $this->luxuryTaxResource->load($object, $id, LuxuryTaxInterface::CUSTOMER_GROUP);
        if (!$object->getId()) {
            throw new NoSuchEntityException(__("Unable to find entity with customer group {$id}"));
        }
        return $object;
    }
}
