<?php
declare(strict_types=1);

namespace Mastering\LuxuryTax\Model\Sales\Pdf;

use Magento\Sales\Model\Order\Pdf\Total\DefaultTotal;

/**
 * @method getFontSize()
 * @method getAmountPrefix()
 * @method getTitle()
 */
class LuxuryTax extends DefaultTotal
{
    /**
     * Get array of arrays with totals information for display in PDF
     * array(
     *  $index => array(
     *      'amount'   => $amount,
     *      'label'    => $label,
     *      'font_size'=> $font_size
     *  )
     * )
     * @return array
     */
    public function getTotalsForDisplay(): array
    {
        $extraFee = $this->getOrder()->getLuxuryTax();
        if ($extraFee === null) {
            return [];
        }
        $amountInclTax = $this->getOrder()->formatPriceTxt($extraFee);
        $fontSize = $this->getFontSize() ? $this->getFontSize() : 7;

        return [
            [
                'amount' => $this->getAmountPrefix() . $amountInclTax,
                'label' => __($this->getTitle()) . ':',
                'font_size' => $fontSize,
            ]
        ];
    }
}