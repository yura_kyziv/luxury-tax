<?php
declare(strict_types=1);

namespace Mastering\LuxuryTax\Model;

use Magento\Framework\Model\AbstractModel;
use Mastering\LuxuryTax\Api\Data\LuxuryTaxInterface;
use Mastering\LuxuryTax\Model\ResourceModel\LuxuryTax as LuxuryTaxResource;

/**
 * Luxury tax model
 */
class LuxuryTax extends AbstractModel implements LuxuryTaxInterface
{

    /**
     * @return mixed|null
     */
    public function getId()
    {
        return $this->_getData(self::ID);
    }

    /**
     * @param mixed $value
     * @return LuxuryTaxInterface
     */
    public function setId($value): LuxuryTaxInterface
    {
        $this->setData(self::ID, $value);
        return $this;
    }

    /**
     * @return bool
     */
    public function getStatus(): bool
    {
        return (bool)(int)$this->getData(self::STATUS);
    }

    /**
     * @param bool $status
     * @return void
     */
    public function setStatus(bool $status): void
    {
        $this->setData(self::STATUS, $status);
    }

    /**
     * @return string
     */
    public function getTaxName(): string
    {
        return $this->getData(self::NAME);
    }

    /**
     * @param string $taxName
     * @return void
     */
    public function setTaxName(string $taxName): void
    {
        $this->setData(self::NAME, $taxName);
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->getData(self::DESCRIPTION);
    }

    /**
     * @param string $description
     * @return void
     */
    public function setDescription(string $description): void
    {
        $this->setData(self::DESCRIPTION, $description);
    }

    /**
     * @return int
     */
    public function getCustomerGroup(): int
    {
        return (int)$this->getData(self::CUSTOMER_GROUP);
    }

    /**
     * @param int $customerGroup
     * @return void
     */
    public function setCustomerGroup(int $customerGroup): void
    {
        $this->setData(self::CUSTOMER_GROUP, $customerGroup);
    }

    /**
     * @return float
     */
    public function getConditionAmount(): float
    {
        return (float)$this->getData(self::CONDUCTION_AMOUNT);
    }

    /**
     * @param float $conditionAmount
     * @return void
     */
    public function setConditionAmount(float $conditionAmount): void
    {
        $this->setData(self::CONDUCTION_AMOUNT, $conditionAmount);
    }

    /**
     * @return int
     */
    public function getTaxRate(): int
    {
        return (int)$this->getData(self::TAX_RATE);
    }

    /**
     * @param int $taxRate
     * @return void
     */
    public function setTaxRate(int $taxRate): void
    {
        $this->setData(self::TAX_RATE, $taxRate);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        parent::_init(LuxuryTaxResource::class);
    }
}
