<?php
declare(strict_types=1);

namespace Mastering\LuxuryTax\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Magento\Framework\Model\ResourceModel\Db\Context;
use Mastering\LuxuryTax\Api\Data\LuxuryTaxInterface;

/**
 * Luxury Tax db connection
 */
class LuxuryTax extends AbstractDb
{
    /**
     * DB table name
     */
    public const TABLE_NAME = "mastering_luxury_tax";

    /**
     * @param Context $context
     */
    public function __construct(Context $context)
    {
        parent::__construct($context);
    }

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(self::TABLE_NAME, LuxuryTaxInterface::ID);
    }
}
