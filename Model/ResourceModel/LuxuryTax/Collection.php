<?php
declare(strict_types=1);

namespace Mastering\LuxuryTax\Model\ResourceModel\LuxuryTax;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Mastering\LuxuryTax\Api\Data\LuxuryTaxInterface;
use Mastering\LuxuryTax\Model\LuxuryTax;
use Mastering\LuxuryTax\Model\ResourceModel\LuxuryTax as LuxuryTaxResource;

/**
 * Collection class for LuxuryTax model
 */
class Collection extends AbstractCollection
{
    /**
     * DB id field name
     */
    protected $_idFieldName = LuxuryTaxInterface::ID;

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(LuxuryTax::class, LuxuryTaxResource::class);
    }
}
