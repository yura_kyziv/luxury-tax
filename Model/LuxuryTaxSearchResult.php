<?php
declare(strict_types=1);

namespace Mastering\LuxuryTax\Model;

use Magento\Framework\Api\SearchResults;
use Mastering\LuxuryTax\Api\LuxuryTaxSearchResultInterface;

class LuxuryTaxSearchResult extends SearchResults implements LuxuryTaxSearchResultInterface
{

}

