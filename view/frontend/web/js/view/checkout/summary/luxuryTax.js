define(
    [
        'Magento_Checkout/js/view/summary/abstract-total',
        'Magento_Checkout/js/model/quote',
        'Magento_Catalog/js/price-utils',
        'Magento_Checkout/js/model/totals'
    ],
    function (Component, quote, priceUtils, totals) {
        "use strict";
        return Component.extend({
            defaults: {
                isFullTaxSummaryDisplayed: window.checkoutConfig.isFullTaxSummaryDisplayed || false,
                template: 'Mastering_LuxuryTax/checkout/summary/luxuryTax'
            },
            totals: quote.getTotals(),
            isTaxDisplayedInGrandTotal: window.checkoutConfig.includeTaxInGrandTotal || false,
            initialize: function () {
                this._super();
                if (this.totals().subtotal == this.totals().grand_total) {
                    totals.getSegment('grand_total').value = +this.totals().grand_total + totals.getSegment('luxuryTax').value;
                }
                return this;
            },
            getValue: function () {
                var price = 0;
                if (this.totals()) {
                    price = totals.getSegment('luxuryTax').value;
                }
                return this.getFormattedPrice(price);
            },
            getBaseValue: function () {
                var price = 0;
                if (this.totals()) {
                    price = this.totals().base_luxuryTax;
                }
                return priceUtils.formatPrice(price, quote.getBasePriceFormat());
            },
        });
    }
);