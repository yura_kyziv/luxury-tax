define(
    [
        'Mastering_LuxuryTax/js/view/checkout/summary/luxuryTax'
    ],
    function (Component) {
        'use strict';

        return Component.extend({

            /**
             * @override
             */
            isDisplayed: function () {
                return true;
            }
        });
    }
);