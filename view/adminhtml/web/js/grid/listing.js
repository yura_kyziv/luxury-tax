define([
    'Magento_Ui/js/grid/listing'
], function (Collection) {
    'use strict';

    return Collection.extend({
        defaults: {
            template: 'Mastering_LuxuryTax/ui/grid/listing'
        },

        getRowClass: function (col, row) {
            if (col.index == 'base_luxury_tax') {
                let count = row.base_luxury_tax
                count = count.replace(/[a-zA-Z]/g, '');
                if (count < 100) {
                    return window.luxuryTaxColorLow;
                } else if (count < 1000) {
                    return window.luxuryTaxColorMid;
                } else if (count >= 1000) {
                    return window.luxuryTaxColorMax;
                }
            }
        }
    });
});
