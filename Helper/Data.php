<?php
declare(strict_types=1);

namespace Mastering\LuxuryTax\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

class Data extends AbstractHelper
{
    const XML_PATH = 'mastering_luxuryTax/';

    /**
     * @param string $color
     * @return mixed
     */
    public function getColor(string $color)
    {
        return $this->scopeConfig->getValue('mastering_luxuryTax/general/' . $color . '_color', ScopeInterface::SCOPE_STORE);
    }

    /**
     * @param string $fieldId
     * @param string|null $storeCode
     * @return mixed
     */
    private function getGeneralConfig(string $fieldId, string $storeCode = null)
    {
        return $this->getConfigValue(self::XML_PATH . 'general/' . $fieldId, $storeCode);
    }

    /**
     * @param string $field
     * @param string|null $storeCode
     * @return mixed
     */
    private function getConfigValue(string $field, string $storeCode = null)
    {
        return $this->scopeConfig->getValue($field, ScopeInterface::SCOPE_STORE, $storeCode);
    }

}
