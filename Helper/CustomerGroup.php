<?php
declare(strict_types=1);

namespace Mastering\LuxuryTax\Helper;

use Magento\Customer\Model\ResourceModel\Group\Collection as CustomerGroups;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Mastering\LuxuryTax\Model\LuxuryTaxRepository;

class CustomerGroup extends AbstractHelper
{
    /**
     * @var CustomerGroups
     */
    protected CustomerGroups $customerGroup;

    /**
     * @var SearchCriteriaBuilder
     */
    private SearchCriteriaBuilder $searchCriteriaBuilder;

    /**
     * @var LuxuryTaxRepository
     */
    private LuxuryTaxRepository $luxuryTaxRepository;

    /**
     * @param Context $context
     * @param CustomerGroups $customerGroup
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param LuxuryTaxRepository $luxuryTaxRepository
     */
    public function __construct(
        Context               $context,
        CustomerGroups        $customerGroup,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        LuxuryTaxRepository   $luxuryTaxRepository
    )
    {
        $this->customerGroup = $customerGroup;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->luxuryTaxRepository = $luxuryTaxRepository;

        parent::__construct($context);
    }

    /**
     * @return array
     */
    public function getCustomerGroups(): array
    {
        $customerGroups = $this->customerGroup->toOptionArray();
        $selectedCustomerGroups = $this->getSelectedCustomerGroups();
        $result = [
            [
                'value' => -1,
                'label' => 'No group'
            ]
        ];
        foreach ($customerGroups as $customerGroup) {
            if (!in_array($customerGroup['value'], $selectedCustomerGroups)) {
                $result[] = $customerGroup;
            }
        }
        return $result;
    }

    /**
     * @return array
     */
    private function getSelectedCustomerGroups(): array
    {
        $filter = $this->searchCriteriaBuilder->setCurrentPage(1)->create();
        $data = $this->luxuryTaxRepository->getList($filter)->getItems();

        $selectedCustomerGroups = [];

        foreach ($data as $item) {
            $customerGroup = $item->getCustomerGroup();
            if ($customerGroup != -1) {
                $selectedCustomerGroups[] = $customerGroup;
            }
        }
        return array_unique($selectedCustomerGroups);
    }
}
