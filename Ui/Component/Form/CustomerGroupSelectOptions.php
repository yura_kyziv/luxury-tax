<?php
declare(strict_types=1);

namespace Mastering\LuxuryTax\Ui\Component\Form;

use Exception;
use Magento\Customer\Model\ResourceModel\GroupRepository;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Option\ArrayInterface;
use Mastering\LuxuryTax\Helper\CustomerGroup;
use Mastering\LuxuryTax\Model\LuxuryTaxRepository;

class CustomerGroupSelectOptions implements ArrayInterface
{

    /**
     * Customer Group
     *
     * @var CustomerGroup
     */
    private CustomerGroup $customerGroup;

    /**
     * @var Http
     */
    private Http $request;

    /**
     * @var LuxuryTaxRepository
     */
    private LuxuryTaxRepository $luxuryTaxRepository;

    /**
     * @var GroupRepository
     */
    private GroupRepository $groupRepository;

    /**
     * @param Http $request
     * @param LuxuryTaxRepository $luxuryTaxRepository
     * @param CustomerGroup $customerGroup
     * @param GroupRepository $groupRepository
     */
    public function __construct(
        Http                $request,
        LuxuryTaxRepository $luxuryTaxRepository,
        CustomerGroup       $customerGroup,
        GroupRepository     $groupRepository
    )
    {
        $this->customerGroup = $customerGroup;
        $this->request = $request;
        $this->luxuryTaxRepository = $luxuryTaxRepository;
        $this->groupRepository = $groupRepository;
    }

    /**
     * @return array
     */
    public function toOptionArray(): array
    {
        $options = $this->getTaxGroup();
        $customerGroups = $this->getCustomerGroups();
        foreach ($customerGroups as $customerGroup) {
            $options[] = $customerGroup;
        }

        return $options;
    }

    /**
     * @return array|array[]
     */
    private function getTaxGroup(): array
    {
        $this->request->getParams();
        $id = $this->request->getParam('id');

        try {
            $luxuryTax = $this->luxuryTaxRepository->get((int)$id);
            $groupId = $luxuryTax->getCustomerGroup();
            $result = [
                [
                    'value' => $groupId,
                    'label' => $this->groupRepository->getById($groupId)->getCode()
                ]
            ];
            return $result;
        } catch (Exception $e) {
            return [];
        }
    }

    /**
     * Get customer groups
     *
     * @return array
     */
    public function getCustomerGroups(): array
    {
        $customerGroups = $this->customerGroup->getCustomerGroups();
        return $customerGroups;
    }
}
