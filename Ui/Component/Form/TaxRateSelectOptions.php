<?php
declare(strict_types=1);

namespace Mastering\LuxuryTax\Ui\Component\Form;

use Magento\Framework\Option\ArrayInterface;
use Magento\Tax\Model\Calculation\Rate;

class TaxRateSelectOptions implements ArrayInterface
{

    /**
     * @var Rate
     */
    protected Rate $taxRates;

    /**
     * @param Rate $taxRates
     */
    public function __construct(
        Rate $taxRates
    )
    {
        $this->taxRates = $taxRates;
    }

    /**
     * @return array
     */
    public function toOptionArray(): array
    {
        $options = [];
        $taxRates = $this->getTaxRates();

        foreach ($taxRates as $taxRate) {
            $options[] = $taxRate;
        }

        return $options;
    }

    /**
     * Get tax rates
     *
     * @return array
     */
    public function getTaxRates(): array
    {
        $taxRates = $this->taxRates->getCollection()->getData();
        $result = [];

        foreach ($taxRates as $taxRate) {
            $rate['label'] = $taxRate['code'];
            $rate['value'] = $taxRate['tax_calculation_rate_id'];
            $result[] = $rate;
        }
        return $result;

    }
}
