<?php
declare(strict_types=1);

namespace Mastering\LuxuryTax\Ui\Component\Listing\Columns;

use Exception;
use Magento\Customer\Model\Customer;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;
use Mastering\LuxuryTax\Model\LuxuryTaxRepository;

class CustomerLuxuryTax extends Column
{

    /**
     * @var Customer
     */
    private Customer $customer;

    /**
     * @var LuxuryTaxRepository
     */
    private LuxuryTaxRepository $luxuryTaxRepository;

    /**
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param Customer $customer
     * @param LuxuryTaxRepository $luxuryTaxRepository
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface    $context,
        UiComponentFactory  $uiComponentFactory,
        Customer            $customer,
        LuxuryTaxRepository $luxuryTaxRepository,
        array               $components = [],
        array               $data = []
    )
    {
        $this->customer = $customer;
        $this->luxuryTaxRepository = $luxuryTaxRepository;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource): array
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $customer = $this->customer->load($item['entity_id']);
                $groupId = $customer->getGroupId();
                try {
                    $luxuryTax = $this->luxuryTaxRepository->getByCustomerGroup((int)$groupId);
                    $luxuryTax = $luxuryTax->getTaxName();
                } catch (Exception $e) {
                    $luxuryTax = "";
                }
                $item['luxury_tax'] = $luxuryTax;
            }
        }
        return $dataSource;
    }
}
