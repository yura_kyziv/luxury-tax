<?php
declare(strict_types=1);

namespace Mastering\LuxuryTax\Ui\Component\Listing\Columns;

use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentInterface;
use Magento\Ui\Component\Listing\Columns;

class OrdersLuxuryTax extends Columns
{

    /**
     * Constructor
     *
     * @param ContextInterface $context
     * @param UiComponentInterface[] $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        array            $components = [],
        array            $data = []
    )
    {
        parent::__construct($context, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource): array
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                if (isset($item['luxury_tax'])) {
                    $item['luxury_tax'] = $this->formatLuxuryTax($item['luxury_tax'], $item['order_currency_code']);
                }
                if (isset($item['base_luxury_tax'])) {
                    $item['base_luxury_tax'] = $this->formatLuxuryTax($item['base_luxury_tax'], $item['base_currency_code']);
                }
            }
        }
        return $dataSource;
    }

    /**
     * @param string $tax
     * @param string $currency
     * @return string
     */
    private function formatLuxuryTax(string $tax, string $currency): string
    {
        $tax = $currency . " " . number_format((float)$tax, 2, '.', '');
        return $tax;
    }
}
