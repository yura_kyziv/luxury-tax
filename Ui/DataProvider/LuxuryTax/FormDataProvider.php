<?php
declare(strict_types=1);

namespace Mastering\LuxuryTax\Ui\DataProvider\LuxuryTax;

use Magento\Ui\DataProvider\AbstractDataProvider;
use Mastering\LuxuryTax\Model\ResourceModel\LuxuryTax\CollectionFactory;

/**
 * luxury tax data provider
 */
class FormDataProvider extends AbstractDataProvider
{
    /**
     * @var CollectionFactory
     */
    private CollectionFactory $collectionFactory;

    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $collectionFactory
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        array $meta = [],
        array $data = []
    )
    {
        $this->collectionFactory = $collectionFactory;
        $this->collection = $collectionFactory->create();
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        if (!$this->getCollection()->isLoaded()) {
            $this->getCollection()->load();
        }
        $items = $this->getCollection()->toArray();

        if (isset($items["items"][0]['status'])) {
            $items["items"][0]['status'] = (bool)(int)$items["items"][0]['status'];
        }

        return $items;
    }
}

