<?php
declare(strict_types=1);

namespace Mastering\LuxuryTax\Ui\DataProvider\LuxuryTax;

use Magento\Customer\Api\GroupRepositoryInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Ui\DataProvider\AbstractDataProvider;
use Mastering\LuxuryTax\Model\ResourceModel\LuxuryTax\CollectionFactory;

/**
 * luxury tax data provider
 */
class ListingDataProvider extends AbstractDataProvider
{
    /**
     * @var CollectionFactory
     */
    private CollectionFactory $collectionFactory;

    /**
     * @var GroupRepositoryInterface
     */
    private GroupRepositoryInterface $groupRepository;

    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $collectionFactory
     * @param GroupRepositoryInterface $groupRepository
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        string                   $name,
        string                   $primaryFieldName,
        string                   $requestFieldName,
        CollectionFactory        $collectionFactory,
        GroupRepositoryInterface $groupRepository,
        array                    $meta = [],
        array                    $data = []
    )
    {
        $this->collectionFactory = $collectionFactory;
        $this->collection = $collectionFactory->create();
        $this->groupRepository = $groupRepository;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
    }

    /**
     * @return array
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function getData(): array
    {
        if (!$this->getCollection()->isLoaded()) {
            $this->getCollection()->load();
        }
        $items = $this->getCollection()->toArray();

        for ($i = 0; $i < $items['totalRecords']; $i++) {
            $customerGroupId = $items["items"][$i]['customer_group'];
            if ($customerGroupId >= 0) {
                $customerGroup = $this->groupRepository->getById($customerGroupId)->getCode();
            } else {
                $customerGroup = "No group";
            }
            $items["items"][$i]['customer_group'] = $customerGroup;

            if ((int)$items["items"][$i]['status']) {
                $items["items"][$i]['status'] = "enable";
            } else {
                $items["items"][$i]['status'] = "disable";
            }

            $items["items"][$i]['condition_amount'] .= '%';
        }
        return $items;
    }
}
