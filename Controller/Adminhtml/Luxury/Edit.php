<?php
declare(strict_types=1);

namespace Mastering\LuxuryTax\Controller\Adminhtml\Luxury;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Mastering\LuxuryTax\Model\LuxuryTaxRepository;

class Edit extends Action
{
    /**
     * @see _isAllowed()
     */
    const ADMIN_RESOURCE = 'Magento_Backend::system';

    /**
     * @var LuxuryTaxRepository
     */
    private LuxuryTaxRepository $productTypesRepository;

    /**
     * @param Context $context
     * @param LuxuryTaxRepository $productTypesRepository
     */
    public function __construct(
        Context             $context,
        LuxuryTaxRepository $productTypesRepository
    )
    {
        parent::__construct($context);
        $this->productTypesRepository = $productTypesRepository;
    }

    /**
     * @return ResultInterface
     */
    public function execute(): ResultInterface
    {
        $id = $this->getRequest()->getParam('id');
        try {
            $type = $this->productTypesRepository->get((int)$id);

            /** @var Page $result */
            $result = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
            $result->setActiveMenu('Magento_Tax::sales_tax')
                ->addBreadcrumb(__('Edit Luxury Tax')->render(), __('Luxury Tax')->render());
            $result->getConfig()
                ->getTitle()
                ->prepend(__("Edit Tax: {$type->getTaxName()}")->render());
        } catch (NoSuchEntityException $e) {
            /** @var Redirect $result */
            $result = $this->resultRedirectFactory->create();
            $this->messageManager->addErrorMessage(
                __("Luxury tax with id {$id} does not exist.")->render()
            );
            $result->setPath('*/*');
        }
        return $result;
    }
}

