<?php
declare(strict_types=1);

namespace Mastering\LuxuryTax\Controller\Adminhtml\Luxury;

use Exception;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\ResultInterface;
use Mastering\LuxuryTax\Api\Data\LuxuryTaxInterface;
use Mastering\LuxuryTax\Api\LuxuryTaxRepositoryInterface;
use Mastering\LuxuryTax\Model\LuxuryTax;
use Mastering\LuxuryTax\Model\LuxuryTaxFactory;

class Save extends Action implements HttpPostActionInterface
{
    /**
     * @var LuxuryTaxRepositoryInterface
     */

    private LuxuryTaxRepositoryInterface $luxuryTaxRepository;

    /**
     * @var LuxuryTaxFactory
     */
    private LuxuryTaxFactory $luxuryTaxFactory;

    /**
     * @param Context $context
     * @param LuxuryTaxRepositoryInterface $luxuryTaxRepository
     * @param LuxuryTaxFactory $luxuryTaxFactory
     */
    public function __construct(
        Context                      $context,
        LuxuryTaxRepositoryInterface $luxuryTaxRepository,
        LuxuryTaxFactory             $luxuryTaxFactory
    )
    {
        parent::__construct($context);
        $this->luxuryTaxRepository = $luxuryTaxRepository;
        $this->luxuryTaxFactory = $luxuryTaxFactory;
    }

    /**
     * @return ResultInterface
     */
    public function execute(): ResultInterface
    {
        $resultRedirect = $this->resultRedirectFactory->create();

        /** @var Http $request */
        $request = $this->getRequest();
        $requestData = $request->getPost()->toArray();

        if (!$request->isPost() || empty($requestData['general'])) {
            $this->messageManager->addErrorMessage(__('Wrong request.')->render());
            $resultRedirect->setPath('*/*/new');
            return $resultRedirect;
        }

        try {
            $id = $requestData['general'][LuxuryTaxInterface::ID];
            $luxuryTax = $this->luxuryTaxRepository->get((int)$id);
        } catch (Exception $e) {
            $luxuryTax = $this->luxuryTaxFactory->create();
        }

        /** @var LuxuryTax $luxuryTax */
        $luxuryTax->setTaxName($requestData['general'][LuxuryTaxInterface::NAME]);
        $luxuryTax->setDescription($requestData['general'][LuxuryTaxInterface::DESCRIPTION]);
        $status = $requestData['general'][LuxuryTaxInterface::STATUS];
        if ($status === 'true') {
            $luxuryTax->setStatus(true);
        } else {
            $luxuryTax->setStatus(false);
        }

        $luxuryTax->setCustomerGroup((int)$requestData['general'][LuxuryTaxInterface::CUSTOMER_GROUP]);
        $luxuryTax->setConditionAmount((float)$requestData['general'][LuxuryTaxInterface::CONDUCTION_AMOUNT]);
        $luxuryTax->setTaxRate((int)$requestData['general'][LuxuryTaxInterface::TAX_RATE]);

        try {
            $luxuryTax = $this->luxuryTaxRepository->save($luxuryTax);
            $this->processRedirectAfterSuccessSave($resultRedirect, $luxuryTax->getId());
            $this->messageManager->addSuccessMessage(__('Luxury tax was saved.')->render());
        } catch (Exception $exception) {
            $this->messageManager->addErrorMessage(__('Error. Cannot save')->render());

            $resultRedirect->setPath('*/*/new');
        }

        return $resultRedirect;
    }

    /**
     * @param Redirect $resultRedirect
     * @param string $id
     * @return void
     */
    private function processRedirectAfterSuccessSave(Redirect $resultRedirect, string $id)
    {
        if ($this->getRequest()->getParam('back')) {
            $resultRedirect->setPath(
                '*/*/edit',
                [
                    LuxuryTaxInterface::ID => $id,
                    '_current' => true,
                ]
            );
        } elseif ($this->getRequest()->getParam('redirect_to_new')) {
            $resultRedirect->setPath(
                '*/*/new',
                [
                    '_current' => true,
                ]
            );
        } else {
            $resultRedirect->setPath('*/*/');
        }
    }
}
