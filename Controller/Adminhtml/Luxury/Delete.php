<?php
declare(strict_types=1);

namespace Mastering\LuxuryTax\Controller\Adminhtml\Luxury;

use Exception;
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Magento\Ui\Component\MassAction\Filter;
use Mastering\LuxuryTax\Model\LuxuryTaxRepository;
use Mastering\LuxuryTax\Model\ResourceModel\LuxuryTax\CollectionFactory;

class Delete extends Action implements HttpPostActionInterface
{
    /**
     * @var CollectionFactory
     */
    public CollectionFactory $collectionFactory;

    /**
     * @var Filter
     */
    public Filter $filter;

    /**
     * @var LuxuryTaxRepository
     */
    private LuxuryTaxRepository $luxuryTaxRepository;

    /**
     * @param Context $context
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     * @param LuxuryTaxRepository $luxuryTaxRepository
     */
    public function __construct(
        Context             $context,
        Filter              $filter,
        CollectionFactory   $collectionFactory,
        LuxuryTaxRepository $luxuryTaxRepository
    )
    {
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->luxuryTaxRepository = $luxuryTaxRepository;
        parent::__construct($context);
    }

    /**
     * @return ResponseInterface | ResultInterface
     */
    public function execute()
    {
        try {
            $collection = $this->filter->getCollection($this->collectionFactory->create());
            $collectionSize = $collection->getSize();

            foreach ($collection as $model) {
                $this->luxuryTaxRepository->deleteById($model->getId());
            }
            $this->messageManager->addSuccessMessage(__("A total of {$collectionSize} tax(s) have been deleted.")->render());
        } catch (Exception $e) {
            $this->messageManager->addErrorMessage(__($e->getMessage())->render());
        }
        /** @var  Redirect $redirect */
        $redirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        return $redirect->setPath('*/*/index');
    }

    /**
     * @return bool
     */
    public function _isAllowed(): bool
    {
        return $this->_authorization->isAllowed('Mastering_StoreLocator::mastering');
    }
}

