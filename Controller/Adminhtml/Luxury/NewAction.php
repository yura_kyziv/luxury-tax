<?php
declare(strict_types=1);

namespace Mastering\LuxuryTax\Controller\Adminhtml\Luxury;

use Magento\Backend\App\Action;
use Magento\Backend\Model\View\Result\Page;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;

class NewAction extends Action
{
    /**
     * @see _isAllowed()
     */
    public const ADMIN_RESOURCE = 'Magento_Backend::system';

    /**
     * @return ResultInterface
     */
    public function execute(): ResultInterface
    {
        /** @var Page $resultPage */
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultPage->setActiveMenu('Magento_Tax::sales_tax')
            ->addBreadcrumb(__('New Tax')->render(), __('List')->render());
        $resultPage->getConfig()->getTitle()->prepend(__('New Tax')->render());

        return $resultPage;
    }
}
